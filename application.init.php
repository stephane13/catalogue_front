<?php
/**
* @package   catalogue_front
* @subpackage
* @author    biotopia.bio
* @copyright 2020 biotopia.bio
* @link      www.biotopia.bio
* @license    All rights reserved
*/
$vendorDir = 'C:/wamp64/www/_common/jelix/lib_1.7.4/vendor/';
require($vendorDir.'autoload.php');

jApp::initPaths(
    __DIR__.'/',
    __DIR__.'/www/',
    __DIR__.'/var/',
    __DIR__.'/var/log/',
    __DIR__.'/var/config/'
);

jApp::setTempBasePath(realpath(__DIR__.'/../temp/catalogue_front/').'/');

require($vendorDir.'jelix_app_path.php');

// Declares here paths of directories containings plugins and modules,
// that are not already declared into composer.json files of Composer packages
jApp::declareModulesDir(array(
    __DIR__.'/modules/'
));
jApp::declarePluginsDir(array(
    __DIR__.'/plugins'
));

