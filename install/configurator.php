<?php
/**
* @package   catalogue_front
* @author    biotopia.bio
* @copyright 2020 biotopia.bio
* @link      www.biotopia.bio
* @license    All rights reserved
*/

require_once (__DIR__.'/../application.init.php');

\Jelix\Scripts\Configure::launch();
