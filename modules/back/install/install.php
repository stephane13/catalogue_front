<?php
/**
* @package   catalogue_front
* @subpackage back
* @author    biotopia.bio
* @copyright 2020 biotopia.bio
* @link      www.biotopia.bio
* @license    All rights reserved
*/


class backModuleInstaller extends \Jelix\Installer\Module\Installer {

    function install(\Jelix\Installer\Module\API\InstallHelpers $helpers) {
        //$helpers->database()->execSQLScript('sql/install');

        /*
        jAcl2DbManager::addRole('my.role', 'back~acl.my.role', 'role.group.id');
        jAcl2DbManager::addRight('admins', 'my.role'); // for admin group
        */
    }
}