<?php
/**
* @package   common
* @subpackage common
* @author    your name
* @copyright 2011 your name
* @link      http://www.yourwebsite.undefined
* @license    All rights reserved
*/

// require_once HOST_ROOT.'_common/JWT/JWT.php';
// use Firebase\JWT\JWT;
define('my_key', "_gfdh_'t3hk$765jhg");

class jSystemRoutine
{
	function getCommonHeader(&$rep, $page, $param = array())
	{
		$rep->addJSLink('/jelix/jquery/jquery.min.js');	
		$rep->addJSLink('/jelix/jquery/ui/jquery-ui.min.js');	
		
		
		$rep->addHeadContent('<meta name="viewport" content="">');
				
$rep->addHeadContent( "<script>		
(function(doc) {
	var addEvent = 'addEventListener',
	    type = 'gesturestart',
	    qsa = 'querySelectorAll',
	    scales = [1, 1],
	    meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];
	function fix() {
		meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
		doc.removeEventListener(type, fix, true);
	}
	if ((meta = meta[meta.length - 1]) && addEvent in doc) {
		fix();
		scales = [.25, 1.6];
		doc[addEvent](type, fix, true);
	}
}(document));
</script>");
		
		


		$rep->addMetaAuthor('Biotopia - Catalogue');
	
		$bootstrap = '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"/>'.PHP_EOL;
		$bootstrap .= '<link rel="stylesheet" type="text/css" href="/css/front.css"/>';
		$bootstrap .= '<link rel="stylesheet" type="text/css" href="/jelix/jquery/ui/jquery-ui.css"/>';
		$icons = '';
		$icons .= '<link rel="apple-touch-icon" sizes="57x57" href="/img/icons/apple-icon-57x57.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="60x60" href="/img/icons/apple-icon-60x60.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="72x72" href="/img/icons/apple-icon-72x72.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="76x76" href="/img/icons/apple-icon-76x76.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="114x114" href="/img/icons/apple-icon-114x114.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="120x120" href="/img/icons/apple-icon-120x120.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="144x144" href="/img/icons/apple-icon-144x144.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="152x152" href="/img/icons/apple-icon-152x152.png">'."\n";
		$icons .= '<link rel="apple-touch-icon" sizes="180x180" href="/img/icons/apple-icon-180x180.png">'."\n";
		$icons .= '<link rel="icon" type="image/png" sizes="192x192"  href="/img/icons/android-icon-192x192.png">'."\n";
		$icons .= '<link rel="icon" type="image/png" sizes="32x32" href="/img/icons/favicon-32x32.png">'."\n";
		$icons .= '<link rel="icon" type="image/png" sizes="96x96" href="/img/icons/favicon-96x96.png">'."\n";
		$icons .= '<link rel="icon" type="image/png" sizes="16x16" href="/img/icons/favicon-16x16.png">'."\n";
		$icons .= '<link rel="manifest" href="/img/icons/manifest.json">'."\n";
		$icons .= '<meta name="msapplication-TileColor" content="#ffffff">'."\n";
		$icons .= '<meta name="apple-mobile-web-app-capable" content="yes" /> '."\n";
		$icons .= '<meta name="apple-mobile-web-app-status-bar-style" content="black" /> '."\n";
		$icons .= '<meta name="msapplication-TileImage" content="/img/icons/ms-icon-144x144.png">'."\n";
		$icons .= '<meta name="theme-color" content="#ffffff">'."\n";
		$icons .= '<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">'."\n";
		$icons .= '<link type="image/x-icon" rel="icon" href="/img/favicon.ico" />'."\n";
		$rep->addHeadContent($icons.$bootstrap.PHP_EOL.'<meta http-equiv="X-UA-Compatible" content="IE=edge">'.PHP_EOL.'<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->');
		
		
		$rep->addJSLink('/js/main.js');
		
		
		$rep->body->assign('URI', $_SERVER['REQUEST_URI']);
		$rep->body->assign('PAGE', $page);
		$rep->body->assign('LANG', substr(jApp::config()->locale,0,2));
		$rep->body->assign('LOCAL', jApp::config()->locale);
		$rep->body->assign('HOST', $_SERVER['HTTP_HOST']);
		
	}
	
	function aMenu($locale, $menu_name='', $smenu_name='')
	{
		$key = 'menu_'.str_replace('~','',$locale).'_'.jApp::config()->locale;
		$aMenuCached = jCache::get($key);
		if($aMenuCached === false )
		{       
			$aMenuCached['aMenu'] = array();
			$aMenuCached['allMenu'] = array();
			$aMenuCached['allsMenu'] = array();
			$nbelem = jLocale::get($locale.'.menu_elem');
			for($i=1;$i<=$nbelem;$i++)
			{
				$ord = intval( jLocale::get($locale.'.menu_ord_'.$i) );
				if($ord>0)
				{
					$elm_sous_menu = jLocale::get($locale.'.menu_elm_'.$i);
					$sMenu = array();
					if($elm_sous_menu > 0)
					{
						for($j=1;$j<=$elm_sous_menu;$j++)
						{
							$sMenu = array(
								'txt'=>jLocale::get($locale.'.smenu_'.$i.'_txt_'.$j),
								'nam'=>jLocale::get($locale.'.smenu_'.$i.'_nam_'.$j),
								'url'=>jLocale::get($locale.'.smenu_'.$i.'_url_'.$j),
								'acl'=>jLocale::get($locale.'.smenu_'.$i.'_acl_'.$j),
								'cla'=>jLocale::get($locale.'.smenu_'.$i.'_cla_'.$j),
								'img'=>jLocale::get($locale.'.smenu_'.$i.'img_'.$j),
								'sty'=>jLocale::get($locale.'.smenu_'.$i.'_sty_'.$j),
								'alt'=>jLocale::get($locale.'.smenu_'.$i.'_alt_'.$j),
								'ord'=>jLocale::get($locale.'.smenu_'.$i.'_ord_'.$j),
								'sMenu'=>array(),
							);
						}
					}
					$aMenuCached['aMenu']['_'.jLocale::get($locale.'.menu_men_'.$i).'_'][ $ord ] = array(
						'txt'=>jLocale::get($locale.'.menu_txt_'.$i),
						'nam'=>jLocale::get($locale.'.menu_nam_'.$i),
						'url'=>jLocale::get($locale.'.menu_url_'.$i),
						'acl'=>jLocale::get($locale.'.menu_acl_'.$i),
						'cla'=>jLocale::get($locale.'.menu_cla_'.$i),
						'img'=>jLocale::get($locale.'.menuimg_'.$i),
						'sty'=>jLocale::get($locale.'.menu_sty_'.$i),
						'alt'=>jLocale::get($locale.'.menu_alt_'.$i),
						'sMenu'=>$sMenu,
					);
					$aMenuCached['allMenu'][] = $aMenuCached['aMenu']['_'.jLocale::get($locale.'.menu_men_'.$i).'_'][ $ord ];
					$aMenuCached['allsMenu']['_'.jLocale::get($locale.'.menu_men_'.$i).'_'.jLocale::get($locale.'.menu_nam_'.$i).'_'] = $sMenu;
				}
			}
			jCache::set($key, $aMenuCached, 60);
		}
		if($smenu_name != '' && isset($aMenuCached['allsMenu']['_'.$menu_name.'_'.$smenu_name.'_']) )
		{
			return $aMenuCached['allsMenu']['_'.$menu_name.'_'.$smenu_name.'_'];
		}
		elseif( isset($aMenuCached['aMenu']['_'.$menu_name.'_']) )
		{
			return $aMenuCached['aMenu'][$menu_name];
		}
		else
		{
			return $aMenuCached['allMenu'];
		}
	}
	
	function get404($rep)
	{
		$rep->setXhtmlOutput(false);
		$rep->setHttpStatus ("404", "Service non disponible");        
		$rep->title = jLocale::get('common~PAGE_system_close.header.title');
		$rep->addMetaDescription( jLocale::get('common~PAGE_system_error404.header.meta.desc') );
		$rep->addMetaKeywords( jLocale::get('common~PAGE_system_error404.header.meta.keyword') );
		$rep->bodyTpl = 'common~PAGE_system_error404';
		return $rep;
	}
	
	function getMeta($page, $meta_name, &$rep, &$info = false)
	{		
		$tmp = jLocale::get($page.'.header.meta.'.$meta_name);
		if($tmp != '')
		{
			if($info !== false)
			{
				$tmp_tag = array('%label%'=>'label','%summary%'=>'summary','%keyword%'=>'keyword','%name%'=>'name');
				foreach($tmp_tag as $k => $v)
				{
					if( isset($info[$v]) )
					{
						$tmp = str_replace($k, substr($info[$v], 0, 150), $tmp);
					}
					else
					{
						$tmp = str_replace($k, '', $tmp);
					}
				}
			}
		
			switch(strtolower($meta_name))
			{
				case 'title': $rep->title = $tmp; break;
				case 'description': $rep->addMetaDescription( $tmp ); break;
				case 'keywords': $rep->addMetaKeywords( $tmp ); break;
				case 'author': $rep->addMetaAuthor($tmp); break;
				default:$rep->addHeadContent('<meta name="'.$meta_name.'" content="'. $tmp.'" />');
			}
		}
	}
	
	function getIsoString(&$str)
	{
		$str = mb_convert_encoding($str, 'ISO-8859-1', mb_detect_encoding($str, "UTF-8, ISO-8859-1, ISO-8859-15", true));
		$str = strtr( $str, 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ', 'AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn');
		return $str;
	}
	
	function getKeywordFromText(&$text)
	{
		$keywords = str_word_count( strip_tags( $this->getIsoString($text) ) , 1);
		$keyword = array();
		foreach($keywords as $val)
		{
			if(strlen($val) >4)
			{
				if(isset($keyword[$val]))
					$keyword[$val]++;
				else
					$keyword[$val] = 1;
			}
		}
		asort($keyword, SORT_NUMERIC);
		if(count($keyword) > 15)
		{
			$keyword = array_slice($keyword, 0, 15, true);
		}
		return utf8_encode(implode( ', ', array_keys($keyword)));
	}

	// Redimensionne l'image et la place dans le dossier
	function resize_pic($source, $new_w, $new_h, $filetype, $target = '' )
	{
		if($target == ''){
			$target = $source;
		}
		$imagedata = getimagesize($source);
		
		$w = $imagedata[0];
		$h = $imagedata[1];
		
		if ($new_w == 0 || $new_h == 0)
		{
			$im2 = ImageCreateTrueColor($w, $h);
			$new_w = $w;
			$new_h = $h;
		}
		else
		{
			$ratio = $w/$h;
			$new_ratio = $new_w/$new_h;
			if($new_ratio > $ratio)
			{
				$new_w = $new_h*$ratio;
			}
			else
			{
				$new_h = $new_w/$ratio;
			}
			
			$im2 = ImageCreateTrueColor($new_w, $new_h);
		}
		
		$filetype = strtolower($filetype);
		if($filetype == 'jpeg' || $filetype == 'jpg')
		{ 
			$image = ImageCreateFromJpeg($source); 
		}
		elseif($filetype == 'gif')
		{
			$image = ImageCreateFromGif($source); 
		}
		elseif($filetype == 'png')
		{ 
			$image = ImageCreateFromPng($source);
						
		}
		else
		{ 
			return false; 
		}

		imagecopyResampled($im2, $image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);
		imagesavealpha($image, true);
		imagepng($im2, $target);
		return true;
	}
	
function resize_pic_jpg($source, $new_w, $new_h, $filetype, $target = '', $watermaque = false )
	{
		if($target == ''){
			$target = $source;
			}
		$imagedata = getimagesize($source);
		
		$w = $imagedata[0];
		$h = $imagedata[1];
		$filetype = $imagedata[2];
		
		if ($w == 0 || $h == 0)
		{
		
			return false; 
		
		}
		if ($new_w == 0 || $new_h == 0)
		{
			$im2 = ImageCreateTrueColor($w, $h);
			$new_w = $w;
			$new_h = $h;
		}
		else
		{
			$ratio = $w/$h;
			$new_ratio = $new_w/$new_h;
			if($new_ratio > $ratio)
			{
				$new_w = $new_h*$ratio;
			}
			else
			{
				$new_h = $new_w/$ratio;
			}
			$new_w=intval($new_w);
			$new_h=intval($new_h);
			$im2 = ImageCreateTrueColor($new_w, $new_h);
		}
		imagesavealpha($im2, true);

    $trans_colour = imagecolorallocatealpha($im2, 0, 0, 0, 127);
    imagefill($im2, 0, 0, $trans_colour);
   
    // $red = imagecolorallocate($im2, 255, 0, 0);
    // imagefilledellipse($im2, 400, 300, 400, 300, $red);
		// $filetype = strtolower($filetype);
		if($filetype == IMAGETYPE_JPEG )
		{ 
			$image = ImageCreateFromJpeg($source); 
		}
		elseif($filetype == IMAGETYPE_GIF)
		{
			$image = ImageCreateFromGif($source); 
		}
		elseif($filetype == IMAGETYPE_PNG)
		{ 
			$image = ImageCreateFromPng($source); 
		}
		else
		{ 
			return false; 
		}
		
		imagesavealpha($image, true);
		imagecopyResampled($im2, $image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);
		
		if($watermaque !== false)
		{// Ajout de watermark
			/*$wm = imagecreatefrompng(jApp::configPath().'/wm_mask.png');
			imagealphablending($wm,false);
			imagesavealpha($wm,true);
			imagecopyresampled($im2,$wm,0,0,0,0,400,400,400,400);*/
		//$couleur = imagecolorallocate($im2,hexdec("FC"),hexdec("FD"),hexdec("FD")); 
		//imagestringup($im2, 1, 1, 300, $watermaque, $couleur);
		}
		imagepng ($im2, $target,7);
		return true;
	}
	
	function addTextToPngFile($pngSrc,$key,$text) {
        $chunk = $this->phpTextChunk($key,$text);
        $png = file_get_contents($pngSrc);
        $png2 = $this->addPngChunk($chunk,$png);
        file_put_contents($pngSrc,$png2);
    }
	
	function phpTextChunk($key,$text) {
        $chunktype = "tEXt";
        $chunkdata = $key . "\0" . $text;
        $crc = pack("N", crc32($chunktype . $chunkdata));
        $len = pack("N",strlen($chunkdata));
        return $len .  $chunktype  . $chunkdata . $crc;
    }
	
	function addPngChunk($chunk,$png) {
        $len = strlen($png);
        return substr($png,0,$len-12) . $chunk . substr($png,$len-12,12);
    }

	
	public function send_email($tpl, $to, $aInfo, $file='')
	{
		$mail = new jMailer();
		$mail->isHTML(true);
		$mail->CharSet = 'UTF-8';
		if (isset($aInfo['_sujet']) )
		{
			$mail->Subject = $aInfo['_sujet'];
		}
		else
		{
			$mail->Subject = jLocale::get($tpl.'.sujet');
		}
		$mail->AddAddress($to);
		
		$mail->From = jLocale::get('common~_config.general.default_sender.email');
		$mail->FromName = jLocale::get('common~_config.general.default_sender.name');
		
		$mailBcc = jLocale::get('common~_config.general.send_copy_bcc.email');
		if ($mailBcc != '' && $mailBcc != $to)
		{
			$mail->AddBCC($mailBcc,jLocale::get('common~_config.general.send_copy_bcc.name'));
		}
		$mailBcc2 = jLocale::get('common~_config.general.send_copy_bcc2.email');
		if ($mailBcc2 != '' && $mailBcc2 != $to)
		{
			$mail->AddBCC($mailBcc2,jLocale::get('common~_config.general.send_copy_bcc2.name'));
		}
		
		if($file != '')
		{
			$file_name = substr($file, (strlen($file) - strrpos($file,'/') * -1 ) );
			$mail->AddAttachment($file, $file_name);
		}
		$_tpl = $mail->Tpl($tpl, true);
		$_tpl->assign('aInfo', $aInfo);
		$_tpl->assign('HOST', $_SERVER['HTTP_HOST']);
		return ($mail->Send());
	}
    	
	private function logEvent($msg, $filename='errors.log')
	{ //TO DO utiliser jLog
		if( is_array($msg) )
			$msg = print_r($msg,true)."\n";
		// Assurons nous que le fichier est accessible en écriture
		if (is_writable($filename))
		{
			// Dans notre exemple, nous ouvrons le fichier $filename en mode d'ajout
			// Le pointeur de fichier est placé à la fin du fichier
			// c'est là que $somecontent sera placé
			if (!$handle = fopen($filename, 'a'))		{exit;}
			// Ecrivons quelque chose dans notre fichier.
			if (fwrite($handle, $msg."\n") === FALSE)	{exit;}
			fclose($handle);
		}
		else
		{
			if (!$handle = fopen($filename, 'a+'))		{exit;}
			if (fwrite($handle, $msg."\n") === FALSE)	{exit;}
			fclose($handle);
		}
	}
	
	function	getJWT($info)
	{
		jClasses::inc("common~api/jwt/jwt");
		$tokenId    = base64_encode("Catalogue_token");
		$issuedAt   = time();
		$notBefore  = $issuedAt;             //Adding 10 seconds
		$expire     = $notBefore + 3600 * 24;            // Adding 60 seconds
		$serverName = "Catalogue"; // Retrieve the server name from config file
		
		/*
		 * Create the token as an array
		 */
		$data = [
			'iat'  => $issuedAt,         // Issued at: time when the token was generated
			'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
			'iss'  => $serverName,       // Issuer
			'nbf'  => $notBefore,        // Not before
			'exp'  => $expire,           // Expire
			'data' => $info              // Data related to the signer user
		];
		
		$jwt = JWT::encode($data, my_key, 'HS512');
		return ($unencodedArray = ['JWT' => $jwt]);
	}
	
	function	checkArgs(&$rep, $args, $args_r)
	{
		$oneFoundOK = [];
		foreach($args as $type => $as)
		{
			if (isset($args_r[$type]) && $args_r[$type] != '')
				foreach($as as $bs){
					if (!isset($args_r[$bs]) || $args_r[$bs] == ''){
						$oneFoundOK[] = false;
						break;
					}
				}
			else
				$oneFoundOK[] = false;
		}
		if (count($oneFoundOK) < count($args))
			return (true);
		else
		{
			$rep->setHttpStatus('400','Bad Request');
			$rep->data = ['error' => 'Parsing Error'];
			return (false);
		}
	}
	
	function	checkJWT(&$rep)
	{
		jClasses::inc("common~api/jwt/jwt");
		$headers = [];
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				if (str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))) ==='Authorization')
					$headers['Authorization'] = $value;
			}
		}
		if (isset($headers['Authorization']))
			list($jwt) = sscanf( $headers['Authorization'], 'Bearer %s');
		else
			return (false);
		if ($jwt)
		{
			try
			{	
				$token = JWT::decode($jwt, my_key, array('HS512'));
				return ($token);
			}
			catch (Exception $e)
			{
				/*
				 * the token was not able to be decoded.
				 * this is likely because the signature was not able to be verified (tampered token)
				 */
				$rep->setHttpStatus('401','Unauthorized');
				$rep->data = ['error' => 'Unauthorized'];
				return (false);
			}
		}
		else
		{
			/*
			 * No token was able to be extracted from the authorization header
			 */
			$rep->setHttpStatus('401','Unauthorized');
			$rep->data = ['error' => 'Unauthorized'];
			return (false);
		}
		//print_r($arr);
		//die();
		//return ($head);
	}
}