<?php
/**
* @package   catalogue_front
* @subpackage common
* @author    biotopia.bio
* @copyright 2020 biotopia.bio
* @link      www.biotopia.bio
* @license    All rights reserved
*/


class commonModuleInstaller extends \Jelix\Installer\Module\Installer {

    function install(\Jelix\Installer\Module\API\InstallHelpers $helpers) {
        //$helpers->database()->execSQLScript('sql/install');

        /*
        jAcl2DbManager::addRole('my.role', 'common~acl.my.role', 'role.group.id');
        jAcl2DbManager::addRight('admins', 'my.role'); // for admin group
        */
    }
}