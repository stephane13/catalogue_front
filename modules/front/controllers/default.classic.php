<?php
/**
	* @package   catalogue_front
* @subpackage front
* @author    biotopia.bio
* @copyright 2020 biotopia.bio
* @link      www.biotopia.bio
* @license    All rights reserved
*/

class defaultCtrl extends jController {
    /**
    *
    */
    function index() {
	
		
		$page = 'PAGE_default_index';
	
        $rep = $this->getResponse('html');
		$oBj = jClasses::getService("common~jSystemRoutine");
		$oBj->getCommonHeader($rep, $page);

		/*
		** Référencement
		*/
		$meta = array(
			'label'   => '',
			'summary' => '',
			'keyword' => '',
		);
		if($meta['keyword'] == '' && $meta['summary'] != '')
		{
			$meta['keyword'] = $oBj->getKeywordFromText($meta['summary']);
		}
		$oBj->getMeta( $page, 'title',			$rep, $meta);
		$oBj->getMeta( $page, 'desc',			$rep, $meta);
		$oBj->getMeta( $page, 'keyword',		$rep, $meta);
		$oBj->getMeta( $page, 'author',			$rep);
		$oBj->getMeta( $page, 'robots',			$rep);
		$oBj->getMeta( $page, 'geo.region',		$rep);
		$oBj->getMeta( $page, 'geo.placename',	$rep);
		$oBj->getMeta( $page, 'geo.position',	$rep);
		$oBj->getMeta( $page, 'ICBM',			$rep);
		$oBj->getMeta( $page, 'DC.Title',		$rep);
		$oBj->getMeta( $page, 'DC.Type',		$rep);
		$rep->bodyTpl = $page;
		
        return $rep;
    }
	
	function comment_ca_marche() {
	
		$page ='PAGE_default_comment_ca_marche';
        $rep = $this->getResponse('html');
		$oBj = jClasses::getService("common~jSystemRoutine");
		$oBj->getCommonHeader($rep, $page);
	
		/*
		** Référencement
		*/
		$meta = array(
			'label'   => '',
			'summary' => '',
			'keyword' => '',
		);
		if($meta['keyword'] == '' && $meta['summary'] != '')
		{
			$meta['keyword'] = $oBj->getKeywordFromText($meta['summary']);
		}
		$oBj->getMeta( $page, 'title',			$rep, $meta);
		$oBj->getMeta( $page, 'desc',			$rep, $meta);
		$oBj->getMeta( $page, 'keyword',		$rep, $meta);
		$oBj->getMeta( $page, 'author',			$rep);
		$oBj->getMeta( $page, 'robots',			$rep);
		$oBj->getMeta( $page, 'geo.region',		$rep);
		$oBj->getMeta( $page, 'geo.placename',	$rep);
		$oBj->getMeta( $page, 'geo.position',	$rep);
		$oBj->getMeta( $page, 'ICBM',			$rep);
		$oBj->getMeta( $page, 'DC.Title',		$rep);
		$oBj->getMeta( $page, 'DC.Type',		$rep);
		$rep->bodyTpl = $page;
		
        return $rep;
    }
	
	
	
	
	function inscription() {
	
		$page ='PAGE_default_inscription';
        $rep = $this->getResponse('html');
		$oBj = jClasses::getService("common~jSystemRoutine");
		$oBj->getCommonHeader($rep, $page);
	
		/*
		** Référencement
		*/
		$meta = array(
			'label'   => '',
			'summary' => '',
			'keyword' => '',
		);
		if($meta['keyword'] == '' && $meta['summary'] != '')
		{
			$meta['keyword'] = $oBj->getKeywordFromText($meta['summary']);
		}
		$oBj->getMeta( $page, 'title',			$rep, $meta);
		$oBj->getMeta( $page, 'desc',			$rep, $meta);
		$oBj->getMeta( $page, 'keyword',		$rep, $meta);
		$oBj->getMeta( $page, 'author',			$rep);
		$oBj->getMeta( $page, 'robots',			$rep);
		$oBj->getMeta( $page, 'geo.region',		$rep);
		$oBj->getMeta( $page, 'geo.placename',	$rep);
		$oBj->getMeta( $page, 'geo.position',	$rep);
		$oBj->getMeta( $page, 'ICBM',			$rep);
		$oBj->getMeta( $page, 'DC.Title',		$rep);
		$oBj->getMeta( $page, 'DC.Type',		$rep);
		$rep->bodyTpl = $page;
		
        return $rep;
    }
	
	function mdp_oublie() {
	
		$page ='PAGE_default_mdp_oublie';
        $rep = $this->getResponse('html');
		$oBj = jClasses::getService("common~jSystemRoutine");
		$oBj->getCommonHeader($rep, $page);
	
		/*
		** Référencement
		*/
		$meta = array(
			'label'   => '',
			'summary' => '',
			'keyword' => '',
		);
		if($meta['keyword'] == '' && $meta['summary'] != '')
		{
			$meta['keyword'] = $oBj->getKeywordFromText($meta['summary']);
		}
		$oBj->getMeta( $page, 'title',			$rep, $meta);
		$oBj->getMeta( $page, 'desc',			$rep, $meta);
		$oBj->getMeta( $page, 'keyword',		$rep, $meta);
		$oBj->getMeta( $page, 'author',			$rep);
		$oBj->getMeta( $page, 'robots',			$rep);
		$oBj->getMeta( $page, 'geo.region',		$rep);
		$oBj->getMeta( $page, 'geo.placename',	$rep);
		$oBj->getMeta( $page, 'geo.position',	$rep);
		$oBj->getMeta( $page, 'ICBM',			$rep);
		$oBj->getMeta( $page, 'DC.Title',		$rep);
		$oBj->getMeta( $page, 'DC.Type',		$rep);
		$rep->bodyTpl = $page;
		
        return $rep;
    }
	
	function essayer_catalogue() {
	
		$page ='PAGE_default_essayer_catalogue';
        $rep = $this->getResponse('html');
		$oBj = jClasses::getService("common~jSystemRoutine");
		$oBj->getCommonHeader($rep, $page);
	
		/*
		** Référencement
		*/
		$meta = array(
			'label'   => '',
			'summary' => '',
			'keyword' => '',
		);
		if($meta['keyword'] == '' && $meta['summary'] != '')
		{
			$meta['keyword'] = $oBj->getKeywordFromText($meta['summary']);
		}
		$oBj->getMeta( $page, 'title',			$rep, $meta);
		$oBj->getMeta( $page, 'desc',			$rep, $meta);
		$oBj->getMeta( $page, 'keyword',		$rep, $meta);
		$oBj->getMeta( $page, 'author',			$rep);
		$oBj->getMeta( $page, 'robots',			$rep);
		$oBj->getMeta( $page, 'geo.region',		$rep);
		$oBj->getMeta( $page, 'geo.placename',	$rep);
		$oBj->getMeta( $page, 'geo.position',	$rep);
		$oBj->getMeta( $page, 'ICBM',			$rep);
		$oBj->getMeta( $page, 'DC.Title',		$rep);
		$oBj->getMeta( $page, 'DC.Type',		$rep);
		$rep->bodyTpl = $page;
		
        return $rep;
    }
	
	function contactez_nous() {
	
		$page ='PAGE_default_contactez_nous';
        $rep = $this->getResponse('html');
		$oBj = jClasses::getService("common~jSystemRoutine");
		$oBj->getCommonHeader($rep, $page);
	
		/*
		** Référencement
		*/
		$meta = array(
			'label'   => '',
			'summary' => '',
			'keyword' => '',
		);
		if($meta['keyword'] == '' && $meta['summary'] != '')
		{
			$meta['keyword'] = $oBj->getKeywordFromText($meta['summary']);
		}
		$oBj->getMeta( $page, 'title',			$rep, $meta);
		$oBj->getMeta( $page, 'desc',			$rep, $meta);
		$oBj->getMeta( $page, 'keyword',		$rep, $meta);
		$oBj->getMeta( $page, 'author',			$rep);
		$oBj->getMeta( $page, 'robots',			$rep);
		$oBj->getMeta( $page, 'geo.region',		$rep);
		$oBj->getMeta( $page, 'geo.placename',	$rep);
		$oBj->getMeta( $page, 'geo.position',	$rep);
		$oBj->getMeta( $page, 'ICBM',			$rep);
		$oBj->getMeta( $page, 'DC.Title',		$rep);
		$oBj->getMeta( $page, 'DC.Type',		$rep);
		$rep->bodyTpl = $page;
		
        return $rep;
    }
	
	function validation() 
	{
	
		$page ='PAGE_default_validation';
        $rep = $this->getResponse('html');
		$oBj = jClasses::getService("common~jSystemRoutine");
		$oBj->getCommonHeader($rep, $page);
	
		/*
		** Référencement
		*/
		$meta = array(
			'label'   => '',
			'summary' => '',
			'keyword' => '',
		);
		if($meta['keyword'] == '' && $meta['summary'] != '')
		{
			$meta['keyword'] = $oBj->getKeywordFromText($meta['summary']);
		}
		$oBj->getMeta( $page, 'title',			$rep, $meta);
		$oBj->getMeta( $page, 'desc',			$rep, $meta);
		$oBj->getMeta( $page, 'keyword',		$rep, $meta);
		$oBj->getMeta( $page, 'author',			$rep);
		$oBj->getMeta( $page, 'robots',			$rep);
		$oBj->getMeta( $page, 'geo.region',		$rep);
		$oBj->getMeta( $page, 'geo.placename',	$rep);
		$oBj->getMeta( $page, 'geo.position',	$rep);
		$oBj->getMeta( $page, 'ICBM',			$rep);
		$oBj->getMeta( $page, 'DC.Title',		$rep);
		$oBj->getMeta( $page, 'DC.Type',		$rep);
		$rep->bodyTpl = $page;
		
        return $rep;
    }
	
	function display() 
	{
	
		$div = $_POST['tpl'];
		$div_available = array(
			'mentions_legales' 	=> 'Mentions legales',
			'login' 			=> 'Identifiez vous',		
		);	
		if(!isset($div_available[$div])){die(json_encode(array('title'=>'Erreur','content'=>'Une erreur est survenu')));}
	
		$tpl = new jTpl();
		$contenu = $tpl->fetch('front~DIV_'.$div);
        $rep = $this->getResponse('json');
		$rep->data = array( 'title'=>$div_available[$div],'content'=>$contenu);
		
		
        return $rep;
    }
	
	function login() 
	{
        $rep = $this->getResponse('text');
			

		$rep->content = jClasses::getService("common~user")->login(
			filter_var($_POST['user_data_login'],FILTER_SANITIZE_EMAIL),
			filter_var($_POST['user_data_pass'],FILTER_SANITIZE_STRING)
		);
			
        return $rep;
    }
	
}

