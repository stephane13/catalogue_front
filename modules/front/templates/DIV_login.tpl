<section>
	<div id="login_form">
		<form id="f_login" class="contacter_ns" action="#" method="post" onsubmit="return login_send('f_login');">
			<div class="form-group">
				<label class="login_form" for="user_data_login">Login<span class="aste">*</span> : </label>
				<input class="form-control" type="email" placeholder="Login" name="user_data_login" id="user_data_login" required="">
			</div>
			<div class="form-group">
				<label class="login_form" for="user_data_pass">Password<span class="aste">*</span> : </label>
				<input class="form-control" type="password" placeholder="Password" name="user_data_pass" id="user_data_pass" required="">
			</div>
			<div class="form-group">
				<label class="login_form" for="user_data_key">Pour les comptes sécurisés par l'application "CMA"<span class="aste">#</span> : </label>
				<input class="form-control" pattern="[0-9]{ldelim}4{rdelim}" type="text" placeholder="Clé" name="user_data_key" id="user_data_key">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-default jaune">Envoyer</button>
			</div>
		</form><!--contacter_ns-->
		<p class="obli">
			<span class="aste">*</span> Ces champs de saisie sont obligatoires<br>
			<span class="aste">#</span> L'application CMA "C'est Moi Autentificator" permet une sécurisation de votre compte par l'utilisation de votre smartphone.
		</p>
	</div>
</section>