{zone 'front~header', array('page' => $PAGE)}
	
	
	
	
	
	<section class="row blockContactezNous">
		<div class="col-lg-4">
			<div class="row">
				<div><h1>Catalogue.Bio</h1> : la plateforme qui vous permet de mettre à jour et accéder à vos information produits en un clic!</div>
				<a href="#" class=" btn-warning_pos1 ">Contactez-nous</a>
			</div>
				
		</div>

		<div class="col-lg-8">
			<img class="rounded mx-auto d-block hidden-xs" src="/img/accueil.svg" alt="catalogueAccueil"/>
		</div>
	
	</section>
	
	<section class="row block">
		<div class="col-lg-12">
			<div class>
				<div>
					<h4><strong>Catalogue.bio</strong></h4> est la base de donnée collaborative de produits biologiques.<br/>
					Cette solution unique sur le marché permet de faciliter l'échange d'informations produits entre<br/>
					les transformateurs et distributeurs.Les informations produits sont ainsi toujours mise à jour.
				</div>
			</div>
		</div>
	</section>
	
	<section class="row mx-auto slide2block">
		<div class="col-lg-5">
			<div class="row">
				<img class="mr-3 ml-3 img1" src="/img/marque-icone.png" alt="je suis une marque">
				<h3>Je suis une marque</h3>
			</div>
					
			<div class="row pt-4">
				<ul>
					<li>Catalogue.bio permet de partager les information sur les produits avec distributeur</li>
					<li>Visualiser l'ensemble de votre catalogue produit</li>
					<li>Accèder à un annuaire magasin</li>
					<li>Exporter des fiches produits au format d'un distributeur.</li>
				</ul>
			</div>
		</div>
				
		<div class="col-lg-1 visible-lg">
			<span class="vertical-line ml-5 h-100"></span>
		</div>
				
		<div class="col-lg-5">
			<div class="row">
				<img class="mr-3 img2 " src="/img/distributeur-icone.png" alt="je suis un distributeur">
				<h3>Je suis un distributeur</h3>
			</div>
			
			<div class="row pt-4" >
				<ul>
					<li>Visualiser les informations complètes sur les assortiments</li>
					<li>Possibilité de faire des benchmark sur de nouveaux produits</li>
					<li>Automatiser le transfert d'information vers un site de click and collect.</li>
							
				</ul>
			</div>
		</div>
	</section>
	
	<div class="lineHorizon"><hr/></div>
	
	<section class="row slide3block">
		<div class="col-lg-4">
			<div>
				<img src="img/collaboratif.png" alt="image collaboratif"/>
				<div class="row">
					<h2>Collaboratif</h2>
					<p>
						La base de données est alimentée par les acteurs de la filière bio.
						C'est autant d'acteurs qui vont contribuer à la richesse des informations disponible.Catalogue.Bio est la richesse des information disponibles.Catalogue.Bio est la référence Européenne en termes d'informations produits.
					</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div>
				<img src="img/qualitatif.png" alt="image qualitatif"/>
				<div class="row">
					<h2>Qualitatif</h2>
					<p>
						L'équipe Biotopia contrôle, modère et vérifie les données produits
						Mise en place d'une solution de versonning pour garantir la fraicheur des informations renseignées.
					</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div>
				<img src="img/participatif.png" alt="image particaltif" />
				<div class="row">
					<h2>Participatif</h2>
					<p>
						Des interfaces et des API tout le temps disponibles permettant la diffusion des informations de manière sychronisées.
						L'information peut alors être complètement ou partiellement partagée avec différents canaux de distribution.
					</p>
				</div>
			</div>
		</div>
	</section>
	
	<section >
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
					<h2>Partenaires</h2>
				</div>
			</div>
					
			<div class="row blockPartenaire">
				<div class="col-lg-12 ">
					<div>
						<img  src="img/logos/logogs1.png" alt="GS1" />
						<img  src="img/logos/logo_les_comptoirs_de_la_bio.png" alt="les comptoirs de la bio" />
						<img  src="img/logos/cosmebio.png" alt= "cosme bio"  />
						<img  src="img/logos/synabio.jpg" alt= "Synabio"  />
						<img  src="img/logos/laviesaine.png" alt="la vie saine" />
					</div>
				</div>
			</div>
					
		</div>
	</section>
	
	<section class="row blockCCMarche">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-12">
					<h2> Comment ça marche ?</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<video class="embed-responsive embed-responsive-16by9" preload="metadata" controls>
						<source src="img/video/catalogue_video_final.mp4" type="video/mp4">
					</video>
				</div>
			</div>
		</div>
	</section>
	
	<section class="row savoirPlus">
		<div class="col-lg-12">
			<div class="row">
				<img class="dimensions img-fluid" src="img/plus.png" alt="en savoir Plus">
				<h3 class ="pt-5 pl-3">En savoir plus</h3>
			</div>
			<div class="row  pl-4 pt-4">
				Pour en savoir plus nous vous proposons de nous contacter pour échanger sur les opportunités qu'offre le projet catalogue.
			</div><br>
					
			<div class="row">
				<ul>
					<li>En savoir plus sur le PIM et comment le projet " Catalogue.bio " peut améliorer la communication</li>
					<li>En savoir plus sur les données disponibles</li>
					<li>En savoir plus sur les outils proposées par " Catalogue.bio "</li>
					<li>En savoir plus sur les opportunités rendues possibles grâce à " Catalogue.bio "</li>
				</ul>
				<a href="#" class="btn-warning_pos2 ml-auto">Contactez-nous</a>
			</div>
		</div>
	</section>
	
	
	



{zone 'front~footer', array('page' => $PAGE)}
