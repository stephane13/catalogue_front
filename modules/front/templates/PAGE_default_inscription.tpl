{zone 'front~header', array('page' => $PAGE)}

	<div class="row">
		<div class="col-xs-4">
			<div class="row">
				<img  src="/img/inscription/catalogue_bio_logo.png" alt="fusée">
			</div>
			<div class="row">
				<img  src="/img/inscription/fusée.svg" alt="fusée">
			</div>
		</div>
		<div class="col-xs-4 pl-5">
		<div>Vous êtes déjà inscrit ? <a href="#">Connectez-vous</a></div>
		<h1>Demande d'inscription Catalogue</h1>
			<form>
				<div class="form-row">
					<div class="form-group col-md-6">
					  <label for="inputNom">Nom</label>
					  <input type="text" class="form-control" id="inputNom">
					</div>
					<div class="form-group col-md-6">
					  <label for="inputPrenom">Prénom</label>
					  <input type="text" class="form-control" id="inputPrenom">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="email" class="form-control" id="inputEmail">
				</div>
				<div class="form-group">
					<label for="inputTelephone">Téléphone</label>
					<input type="number" class="form-control" id="inputTelephone">
				</div>
				<div class="form-group">
					<label for="inputPassword">Password</label>
					<input type="password" class="form-control" id="inputPassword">
				</div>
			  
				<div class="form-group">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" id="gridCheck">
						<label class="form-check-label" for="gridCheck">
						j'accepte <a href="#">les conditions génétales d'utilisation</a>
						</label>
					</div>
				</div>
			  <button type="submit" class="btn btn-primary">Suivant</button>
			</form>
		</div>
	</div>
	


{zone 'front~footer', array('page' => $PAGE)}