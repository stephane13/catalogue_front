		<footer class="row">
			<div class="col-lg-12">
				<div class="row pl-5"><img src="/img/logo.png" alt="Logo catalogue.bio"/></div>
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center">
		
							Catalogue.bio - Biotopia 2016 - 2020&#9426; Recommandé par le synabio & Cosmebio<br/>
							
							<a onclick="afficher('mentions_legales');return false;" href="javascript:">Mentions légales</a>
						</p>
					</div>						
				</div>
			</div>
		</footer>
	</div><!-- .page_front -->
</div><!-- .container -->


	
	
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>


<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" >
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Modal title</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>
		  <div class="modal-body"></div>
		</div>
	  </div>
	</div>
	<script>{literal}
	
	$('#myModal').modal('hide');			
	
	</script>{/literal}