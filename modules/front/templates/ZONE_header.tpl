
	
	<div class="container-fluid">
		<div class="page_front">		
			<header class="row">			
				<div class="col-lg-12">					
					<nav class="navbar navbar-expand-lg navbar-light bg-light">						
						<a class="navbar-brand" href="/">
							<img src="/img/logo.png" width="300" alt="Logo catalogue.bio" />
						</a>							
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>		
						<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
							<ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
							  <li class="nav-item active">
								<a class="nav-link" href="/contactez_nous" data-toggle="modal" data-target="#Modal">Contactez-nous<span class="sr-only">(current)</span></a>
							  </li>
							  <li class="nav-item">
								<a class="nav-link" href="/essayer_catalogue">Essayer Catalogue.bio</a>
							  </li>

							  <li class="nav-item">
								<a class="nav-link" href="/comment_ca_marche">Comment ça marche ?</a>
							  </li>
							  
							</ul>
							<div class="form-inline my-2 my-lg-0 pr-1">
							  <a onclick="afficher('login');return false;" href="javascript:" class="btn btn-outline-dark rounded-pill m-2">Se connecter</a>
							  <a href="/inscription" class="btn btn btn-warning rounded-pill">Inscription</a>
							</div>
						</div>
					</nav>
				</div>			
			</header>

			
			
	
	
	
	