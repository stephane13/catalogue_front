function formQueryString(formId)
{
	var form=$('#'+formId+' input, #'+formId+' select, #'+formId+' textarea');
	queryString='';
	form.each(function () {
		if((($(this).attr('type') != 'radio' && $(this).val() !== '' )||($(this).attr('type') == 'radio' && $(this).is(':checked'))) && (($(this).attr('type') != 'checkbox' && $(this).val() !== '' ) || ($(this).attr('type') == 'checkbox' && $(this).is(':checked'))))
		{
			if(queryString != ''){queryString = queryString + '&';}
			queryString = queryString + encodeURIComponent($(this).attr('name')) + '=' + encodeURIComponent($(this).val())
		}

	});
	return queryString;
}

function is_mail(email)
{
	if (email == ''){ return false; }
	var verif_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-\.]{2,}[\.][a-zA-Z]{2,3}$/;
	if (verif_email.exec(email) == null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@0-mail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@10minutemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@20minutemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@2prong[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@anonimity[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@anoninbox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@anonymbox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@blockfilter[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@briefemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@bugmenot[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@controlpanic[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@cosmorph[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@dandikmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@deadaddress[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@dodgeit[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@dontreg[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@dumpyemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@e4ward[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@emailmiser[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@ephemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@fakedemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@greensloth[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@guerillamail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@haltospam[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@hidzz[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@hushmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@incognitomail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@ipoo[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@jetable[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@kasmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@link2mail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@litedrop[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mailcatch[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@maileater[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mailexpire[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mailinator[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mailzilla[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mierdamail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mintemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mytempemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@mytrashmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@nobulk[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@pookmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@rapidmailbox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@regbypass[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@saynotospams[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@selfdestructingmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@skeefmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@sneakemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@soodonims[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spam[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spamavert[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spambox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spambox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spamcero[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spamfree24[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spamgournet[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spamhole[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spamify[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spaml[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spammotel[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@spamobox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@tempemail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@tempe-mail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@tempinbox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@tempomail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@temporaryinbox[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@undisposable[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
    verif_email = /^[a-zA-Z0-9_\.\-]+@yopmail[\.][a-zA-Z\.]{2,6}$/;
	if (verif_email.exec(email) != null){ return false; }
	return true;
}


function validUser(mail, valid)
{

	 $.ajax({
				type: "POST",
				url: "/ajx-valid.html",
				data		    : "mail="+encodeURIComponent(mail)+"&valid="+encodeURIComponent(valid),
				cache: false,
				complete :  function (data)
				{
                    if(data.responseText == 'OK')
                    {
                        alert('Compte validé, vous pouvez vous connecter avec vos identifiants et mots de passe');
                    }
					if(data.responseText == 'dejafait')
                    {
                        alert('Ce compte a déjà été validé');
                    }
                    if((data.responseText != 'dejafait') && (data.responseText != 'OK'))
                    {
                       alert('Erreur');
                    }
				}
            });
}

function login_send(divID) {
    $.ajax({
        type: "POST",
        url: "/login.ajx",
        data: formQueryString(divID),
        cache: false,
        complete: function ajx_login_send(data) {
            if (data.responseText.substr(0, 3) == 'OK:') {
                $('#f_login').attr('onsubmit', '');
                $('#f_login').attr('action', data.responseText.substr(3));
                $('#f_login').submit();
            } else if (data.responseText == 'KO:NOVALID') {
                $('#f_login').attr('onsubmit', '');
                $('#f_login').attr('action', '/back/');
                $('#f_login').submit();
            } else {			
                $('#user_data_pass').css('border-color', 'red');
                $('#user_data_pass').effect("shake");
            }
        }
    });
    return false;
}

function afficher(divname)
	{		
		$.ajax({
				type: "POST",
				url: "/display.ajx",
				data		    : "tpl="+encodeURIComponent(divname),
				cache: false,
				complete : function ajx_recovery_send(data)
				{
					var t = eval("("+data.responseText+")");
					$('#myModal .modal-title').html(t.title);
					$('#myModal .modal-body').html(t.content);
                    $('#myModal').modal('show');					
				}
            });		
	}